#!/usr/bin/env python
from flask import Flask, request, session, g, redirect, url_for, \
	render_template
import logging
import redis

app = Flask(__name__)
app.config.from_pyfile('/etc/reviewbot/main.cfg')

from controllers.admin import admin
app.register_module(admin, url_prefix='/admin')

from controllers.review import review
app.register_module(review, url_prefix='/review')
