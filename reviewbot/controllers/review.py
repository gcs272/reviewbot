#!/usr/bin/env python
from flask import Flask, g, request, render_template, url_for, Module, \
	flash, session

import redis
from reviewbot import app
from reviewbot.models import repo

import pygments
from pygments.lexers import get_lexer_for_filename, guess_lexer_for_filename
from pygments.formatters import HtmlFormatter
from pygments.styles import get_style_by_name

review = Module(__name__)

@review.before_request
def before_request():
	g.conn = redis.Redis(app.config['REDIS_HOST'])

@review.after_request
def after_request(response):
	g.conn = None
	return response

@review.route('/<repo_id>/')
def commits_index(repo_id):
	r = repo.Repo(repo_id)
	return render_template('review/commits.html', repo=r)

@review.route('/<repo_id>/<commit_id>/')
def review_commit(repo_id, commit_id):
	r = repo.Repo(repo_id)
	commit = r.get_commit(commit_id)
	files = get_files_from_commit(commit)

	blocks = {}
	formatter = HtmlFormatter(style='monokai', linenos=True)
	for path in files:
		fname = path.split('/')[-1:][0]
		try:
			lexer = get_lexer_for_filename(path)
		except:
			try:
				lexer = guess_lexer_for_filename(path, open(path).read())
			except:
				lexer = get_lexer_for_filename('file.txt')

		blocks[fname] = pygments.highlight(
			open(path).read(),
			lexer,
			formatter
		)

	return render_template('review/review.html', commit=commit, blocks=blocks, \
		css=formatter.get_style_defs('.highlight'))

def get_files_from_commit(commit):
	return commit.stats.files.keys()
