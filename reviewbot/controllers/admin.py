#!/usr/bin/env python
from flask import Flask, request, session, g, redirect, url_for, \
	render_template, flash, Module
import logging
import redis
from reviewbot import app
from reviewbot import utility
from reviewbot import models
from reviewbot.models import user, repo

admin = Module(__name__)

@admin.before_request
def before_request():
	g.conn = redis.Redis(app.config['REDIS_HOST'])

@admin.after_request
def after_request(response):
	g.conn = None
	return response

@admin.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'GET':
		return render_template('login.html')
	else:
		if g.conn.hexists('login', request.form['email']) == False:
			flash('Invalid login/password, please try again')
			return render_template('login.html')
		
		passhash = g.conn.hget('login', request.form['email'])
		if passhash is None or passhash != utility.hash(request.form['password']):
			flash('Invalid login/password, please try again')
			return render_template('login.html')

		session['email'] = request.form['email']
		return redirect(url_for('index'))

@admin.route('/logout')
def logout():
	session.pop('email', None)
	flash('Logged out successfully')
	return redirect(url_for('login'))

@admin.route('/')
def index():
	if 'email' not in session:
		return redirect(url_for('login'))

	# Load up repos admined by this account
	u = user.User(session['email'])
	repos = u.get_owned_repos()
	
	return render_template('admin/index.html', repos=repos)

@admin.route('/addrepo', methods=['POST'])
def addrepo():
	r = repo.Repo()
	r.create(request.form['name'], request.form['path'])
	flash('Added repo %s' % (request.form['name']))
	return redirect(url_for('index'))

@admin.route('/edit/<repo_id>/')
def edit(repo_id):
	r = repo.Repo(repo_id)
	return render_template('admin/edit.html', repo=r)
