#!/usr/bin/env python
from reviewbot.models import model
from flask import g
import uuid
import git
import json

class Repo(model.Model):
	def __init__(self, id=None):
		if id is not None:
			self.id = id
			self.inflate(g.conn.hget('repos', self.id))

	def get_id(self):
		return self.id

	def get_name(self):
		return self.name
	
	def get_clone_url(self):
		return self.clone_url

	def get_commit(self, commit_id):
		r = git.Repo(self.local_path)
		for commit in r.iter_commits():
			if str(commit) == commit_id:
				return commit

	def get_all_commits(self):
		r = git.Repo(self.local_path)
		commits = []

		commit = r.heads.master.commit
		commits.append(commit)
		while len(commit.parents) > 0:
			commit = commit.parents[0]
			commits.append(commit)

		return commits

	def create(self, name, local_path):
		repo = {
			'id': str(uuid.uuid4()),
			'name': name,
			'clone_url': None,
			'local_path': local_path
		}

		g.conn.hset('repos', repo['id'], json.dumps(repo))
