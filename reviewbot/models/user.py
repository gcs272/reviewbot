#!/usr/bin/env python
from reviewbot.models import model, repo
from flask import g
import redis

class User(model.Model):
	def __init__(self, email):
		self.email = email
		self.owned_repos = None
		self.inflate(g.conn.hget('users', email))
	
	def get_owned_repos(self):
		repos = []
		
		if self.owned_repos is None:
			return repos

		for repo_id in self.owned_repos:
			repos.append(repo.Repo(repo_id))
		return repos

	def add_owned_repo(self, repo_id):
		self.owned_repos.append(repo_id)
		self.save

	def save(self):
		g.conn.hset('users', self.email, json.dumps({
			'email': self.email,
			'owned_repos': self.owned_repos
		}))
