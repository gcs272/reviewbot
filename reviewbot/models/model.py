#!/usr/bin/env python
from flask import g
import redis
import json

class Model:
	def inflate(self, jsonstr):
		if jsonstr is None:
			return

		m = json.loads(jsonstr)
		for k,v in m.items():
			setattr(self, k, v)
