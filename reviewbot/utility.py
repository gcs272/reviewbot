#!/usr/bin/env python
import hashlib
import hmac
from reviewbot import app

def hash(message):
	return hmac.new(app.config['SECRET_KEY'], message, hashlib.sha256).hexdigest()
